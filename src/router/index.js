import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/pages/Login'
import Home from '@/pages/Home'
import Seek from '@/pages/Seek'
import Adopt from '@/pages/Adopt'
import Feedback from '@/pages/Feedback'
import Complain from '@/pages/Complain'
import Nav from '@/components/Nav'
import Manage from '@/components/Manage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/animal',
      name: 'nav',
      component: Nav,
      redirect: 'animal/home',
      children: [{
        path: 'home',
        name: 'Home',
        component: Home
      }, {
        path: 'manage',
        component: Manage,
        redirect: 'manage/adopt',
        children: [{
          path: 'adopt',
          component: Adopt
        }, {
          path: 'seek',
          component: Seek
        }]
      }, {
        path: 'complain',
        name: 'Complain',
        component: Complain
      }, {
        path: 'feedback',
        name: 'Feedback',
        component: Feedback
      }]
    }
  ]
})
