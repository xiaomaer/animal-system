// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'

Vue.use(VueResource)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  // 监听路由检查登录
  watch: {
    '$route': 'watchLogin'
  },
  created () {
    this.checkLogin()
  },
  methods: {
    checkLogin () {
      let sessionID = sessionStorage.getItem('REQUEST_HEADER_SESSION_ID')
      if (sessionID) {
        // 如果已经登录跳转到登录后的页面
        this.$router.push('/animal')
      } else {
        // 如果没有登录状态则跳转到登录页
        this.$router.push('/login')
      }
    },
    watchLogin () {
      let sessionID = sessionStorage.getItem('REQUEST_HEADER_SESSION_ID')
      if (!sessionID) {
        // 如果没有登录状态则跳转到登录页
        this.$router.push('/login')
      }
    }
  }
})
