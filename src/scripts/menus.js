const menus = [
  {
    menu_icon: 'icon-dashboard',
    menu_name: '发布统计',
    menu_url: '/animal/home',
    sub_menus: []
  }, {
    menu_icon: 'icon-shenhe',
    menu_name: '审核管理',
    menu_url: '/animal/manage',
    sub_menus: [
      {
        sub_name: '领养审核',
        sub_url: '/animal/manage/adopt'
      }, {
        sub_name: '寻找审核',
        sub_url: '/animal/manage/seek'
      }
    ]
  },
  {
    menu_icon: 'icon-fankui',
    menu_name: '反馈处理',
    menu_url: '/animal/feedback',
    sub_menus: []
  },
  {
    menu_icon: 'icon-tousu',
    menu_name: '举报处理',
    menu_url: '/animal/complain',
    sub_menus: []
  }
]
export default menus
